<?php
require_once 'interfaces/HasNameEmail.php';
require_once 'classes/Guest.php';
require_once 'classes/Person.php';
require_once 'classes/Administrator.php';
require_once 'classes/User.php';
require_once 'classes/Writer.php';
require_once 'classes/JsonWriter.php';
require_once 'classes/HtmlWriter.php';



$adminObj = [];
$adminObj[] = new Administrator('John', 'john@gmail.com', '123123', 'Monday');
$adminObj[] = new Administrator('Bob', 'bob@gmail.com', '123123', 'Tuesday');

$userObj = [];
$userObj[] = new User('Jack', 'jack@gmail.com', '123123', '15 May 2020');
$userObj[] = new User('Susan', 'susan@gmail.com', '123123', '14 May 2020');

$guestObjs = [];
$guestObjs[] = new Guest('Billy', 'billy@gmail.com');
$guestObjs[] = new Guest('Jessica', 'jessica@gmail.com');


$jsonWriter = new JsonWriter();

foreach ($adminObj as $admin) {
    $jsonWriter->addUser($admin);
}

foreach ($userObj as $user) {
    $jsonWriter->addUser($user);
}

echo $jsonWriter->write();

//--------------------
echo '<br>';

$htmlWriter = new HtmlWriter();

foreach ($adminObj as $admin) {
    $htmlWriter->addUser($admin);
}

foreach ($userObj as $user) {
    $htmlWriter->addUser($user);
}

foreach ($guestObjs as $guest){
    $htmlWriter->addUser($guest);
}

echo $htmlWriter->write();




