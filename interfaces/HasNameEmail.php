<?php
interface HasNameEmail{

    public function getName();

    public function getEmail();

}