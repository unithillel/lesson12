<?php
require_once 'Person.php';

class User extends Person
{
    protected $registrationDate;

    public function __construct($name, $email, $phone, $registrationDate)
    {
        parent::__construct($name, $email, $phone);
        $this->registrationDate = $registrationDate;
    }

   public function canAccessCabinet(){
        return true;
    }

    public function canAccessControlPanel(){
        return false;
    }

}