<?php


abstract class Writer
{
    protected $users = [];

    public function addUser(HasNameEmail $user){
        $this->users[] = $user;
    }

    abstract public function write();

}