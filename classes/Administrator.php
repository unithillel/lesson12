<?php
require_once 'Person.php';

class Administrator extends Person
{
    protected $dutyDay;

    public function __construct($name, $email, $phone, $dutyDay)
    {
        parent::__construct($name, $email, $phone);
        $this->dutyDay = $dutyDay;
    }

    public function canAccessCabinet(){
        return true;
    }

    public function canAccessControlPanel(){
        return true;
    }

}