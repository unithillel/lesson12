<?php


class JsonWriter extends Writer
{
    public function write()
    {
        $userArr = [];
        foreach ($this->users as $user){
            $userArr[] = [
                'name' => $user->getName(),
                'email' => $user->getEmail(),
            ];
        }
        return json_encode($userArr);
    }
}