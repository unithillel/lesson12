<?php


class HtmlWriter extends Writer
{

    public function write()
    {
        $response = "";
        foreach ($this->users as $user){
            $response.= '<div>';
            $response.= '<ul>';
            $response.= '<li>'.$user->getName().'</li>';
            $response.= '<li>'.$user->getEmail().'</li>';
            $response.= '</ul>';
            $response.= '</div>';

        }
        return $response;
    }
}